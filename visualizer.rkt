#!/usr/bin/env racket
#lang scheme/base

(require racket/gui)
(require racket/draw)
(require racket/list)
(require racket/math)
(require plot)

; task structure
(define (task->graph task)
  (caar task))

(define (task->max-weight task)
  (cadar task))

(define (task->edges task)
  (caddar task))

(define (task->is-required task edge)
  (let ([edge (if (null? (cddr edge))
                edge
                (list (car edge) (cadr edge)))])
    (or (not (equal? #f (member edge (task->edges task))))
        (not (equal? #f (member (reverse edge) (task->edges task)))))))

(define (task->vertices task)
  (cadr task))

(define (task->neighbours task)
  (caddr task))

(define (make-task graph weight edges)
  ; makes a list of (v . (neighbours)) pairs
  ; neighbour is (v weight is-required)
  (define (graph->neighbours graph vertices)
    (define (task->is-required edge)
      (or (not (equal? #f (member edge graph)))
          (not (equal? #f (member (reverse edge) graph)))))

    (map (lambda (v)
           (cons v (map (lambda (e)
                          (if (equal? (car e) v)
                            (list (cadr e) (caddr e) (task->is-required (list (cadr e) v)))
                            (list (car e) (caddr e) (task->is-required (list (car e) v)))))
                        (filter (lambda (e) (or (equal? (car e) v)
                                                (equal? (cadr e) v)))
                                graph))))
         vertices))

  (let ([vertices (sort (remove-duplicates (filter symbol? (flatten graph))) symbol<?)])
   (list (list graph weight edges)
         vertices
         (graph->neighbours graph vertices))))

(define (task->get-weight task beg end)
  (let ([r (filter (lambda (e) (or (and (equal? (car e) beg)
                                        (equal? (cadr e) end))
                                   (and (equal? (car e) end)
                                        (equal? (cadr e) beg))))
                   (task->graph task))])
    (if (null? r)
      #f
      (caddar r))))

; solution structure
(define (sol->ans sol)
  (car sol))

(define (sol->weight sol)
  (cadr sol))

(define (sol->path sol)
  (caddr sol))

(define (make-sol ans . tail)
  (if ans
    (list ans (car tail) (cadr tail))
    (list #f '() '())))

; vertice structure
(define (make-vertice smb cx cy)
  (list (symbol->string smb) cx cy))

(define (vertice->smb v)
  (string->symbol (car v)))

(define (vertice->name v)
  (car v))

(define (vertice->cx v)
  (cadr v))

(define (vertice->cy v)
  (caddr v))

(define (smb->vertice smb vertices)
  (let ([str (symbol->string smb)])
   (findf (lambda (v)
            (equal? str (vertice->name v)))
          vertices)))

; chromosome structure
(define (make-chromosome task path i)
  (define (get-weights path result)
    (if (null? (cdr path))
      (reverse result)
      (get-weights (cdr path) (cons (task->get-weight task (car path) (cadr path)) result))))

  (define (calc-fitness weights)
    (define (edges-included path edges result)
      (define (path-contains-edge? path edge)
        (if (or (null? path) (null? (cdr path)))
          #f
          (let ([a (car path)]
                [b (cadr path)]
                [c (car edge)]
                [d (cadr edge)])
            (cond [(and (equal? a c)
                        (equal? b d))
                   #t]
                  [(and (equal? a d)
                        (equal? b c))
                   #t]
                  [else
                   (path-contains-edge? (cdr path) edge)]))))

      (if (null? edges)
        result
        (edges-included path
                        (cdr edges)
                        (+ result (if (path-contains-edge? path (car edges))
                                    1
                                    0)))))

    (let* ([broken (foldl (lambda (new old)
                            (if (not new)
                              (+ old 1)
                              old))
                          0
                          weights)]
           [edges-not-included (- (length (task->edges task)) (edges-included path (task->edges task) 0))]
           [len-sum (foldl (lambda (new old)
                             (if new
                               (+ old new)
                               old))
                           0
                           weights)])
      (- (if (not (= len-sum 0))
           (/ 1 len-sum)
           0)
         broken
         edges-not-included)))

  (let ([weights (get-weights path '())])
   (list (calc-fitness weights)
         path
         weights
         i)))

(define (chromosome->path c)
  (cadr c))

(define (chromosome->weights c)
  (caddr c))

(define (chromosome->fitness c)
  (car c))

(define (chromosome->i c)
  (cadddr c))

(define CHROMOSOME-CANVAS-H 400)
(define CHROMOSOME-CANVAS-W 400)

(define GRAPH-CANVAS-H 600)
(define GRAPH-CANVAS-W 600)

(define (draw-graph task dc #:is-task? [is-task? #f] #:solution [cycle #f] #:path [path #f] #:path2 [path2 #f] #:text-font [text-font 9] #:left-top [left-top #f])
  (define CANVAS-H (let-values ([(w h) (send dc get-size)]) h))
  (define CANVAS-W (let-values ([(w h) (send dc get-size)]) w))
  (define VERTICE-CIRCLE-RADIUS 4)
  ;(define VERTICE-RECTANGLE-H 100)
  ;(define VERTICE-RECTANGLE-W 100)
  (define GRAPH-RADIUS (* 0.4 (min CANVAS-H CANVAS-W)))
  (define GRAPH-CENTER-X (/ CANVAS-W 2))
  (define GRAPH-CENTER-Y (/ CANVAS-H 2))
  (define MAX-EGDE-WEIGHT 20)
  (define MAX-EGDE-WIDTH 6)

  (let* ([graph (task->graph task)]
         [v-names (task->vertices task)]
         [v-num (length v-names)]
         [step (/ (* 2 pi) v-num)]
         [vertices (foldl (lambda (v i result)
                            (cons (make-vertice v (+ GRAPH-CENTER-X (* GRAPH-RADIUS (cos (* i step))))
                                                (+ GRAPH-CENTER-Y (* GRAPH-RADIUS (sin (* i step)))))
                                  result))
                          '()
                          v-names
                          (build-list v-num values))])

    (define (set-appropriate-font size)
      (send dc set-font (send the-font-list find-or-create-font size 'decorative 'normal 'normal)))

    (define (set-appropriate-pen edge? #:weight [weight 0] #:req? [req? #f] #:in-path? [in-path? #f] #:exists? [exists? #t] #:cycle? [cycle? #t] #:v2? [v2? #f])
      (if edge?
        (if in-path?
          (send dc set-pen (if (not cycle?)
                             (if v2?
                               (make-object color% 0 0 255 0.5)
                               (make-object color% 255 0 0 0.5))
                             (send the-color-database find-color (if exists? "MidnightBlue" "Magenta")))
                           (if (not cycle?) 8 2)
                           'short-dash)
          (send dc set-pen (send the-color-database find-color (if req? "DodgerBlue" "Light Blue"))
                (+ 3 (* MAX-EGDE-WIDTH (/ weight MAX-EGDE-WEIGHT)))
                'solid))
        (send dc set-pen (send the-color-database find-color "Black") 4 'solid)))

    ; draws a circle of vertices
    (define (draw-vertices #:names? [draw-names? #t])
      (define (draw-vertice v)
        (let* ([name (vertice->name v)]
               [cx (vertice->cx v)]
               [cy (vertice->cy v)]
               [circle-x (- cx VERTICE-CIRCLE-RADIUS)]
               [circle-y (- cy VERTICE-CIRCLE-RADIUS)]
               [text-x (+ GRAPH-CENTER-X (* 1.1 (- cx GRAPH-CENTER-X)))]
               [text-y (+ GRAPH-CENTER-Y (* 1.1 (- cy GRAPH-CENTER-Y)))])
          #|(send dc draw-rectangle (- cx VERTICE-RECTANGLE-W)
                  (- cy VERTICE-RECTANGLE-H)
                  (/ VERTICE-RECTANGLE-W 2)
                  (/ VERTICE-RECTANGLE-H 2))|#
          (send dc draw-ellipse circle-x circle-y (* 2 VERTICE-CIRCLE-RADIUS) (* 2 VERTICE-CIRCLE-RADIUS))
          (set-appropriate-font 9)
          (if draw-names? (send dc draw-text name text-x text-y) #t)
          (set-appropriate-font 13)
          ))

      (set-appropriate-pen #f)
      (for-each draw-vertice vertices))

    ; connects vertices with each other
    ; draws weights
    (define (draw-edges #:draw-weights? [draw-weights? #t])
      (define (draw-edge e req?)
        (let* ([v0 (smb->vertice (car e) vertices)]
               [v1 (smb->vertice (cadr e) vertices)]
               [v0x (vertice->cx v0)]
               [v1x (vertice->cx v1)]
               [v0y (vertice->cy v0)]
               [v1y (vertice->cy v1)]
               [weight (caddr e)]
               [weight-pos-x (+ v0x (* 1/3 (- v1x v0x)))]
               [weight-pos-y (+ v0y (* 1/3 (- v1y v0y)))])
          (set-appropriate-pen #t #:weight weight #:req? req?)
          (send dc draw-line v0x v0y v1x v1y)
          (if draw-weights?
            (begin (set-appropriate-font 8)
                   (send dc draw-text (number->string weight) weight-pos-x weight-pos-y)
                   (set-appropriate-font 13))
            #t)))

      (for-each (lambda (e) (draw-edge e (task->is-required task e))) graph))

    (define (draw-path path cycle? #:v2? [v2? #f])
      (define (draw-edge v0 v1)
        (let* ([v0x (vertice->cx v0)]
               [v1x (vertice->cx v1)]
               [v0y (vertice->cy v0)]
               [v1y (vertice->cy v1)]
               [exists? (task->get-weight task (vertice->smb v0) (vertice->smb v1))])
          (set-appropriate-pen #t #:in-path? #t #:exists? exists? #:cycle? cycle? #:v2? v2?)
          (send dc draw-line v0x v0y v1x v1y)))

      (if (null? (cdr path))
        #t
        (begin
          (draw-edge (smb->vertice (car path) vertices) (smb->vertice (cadr path) vertices))
          (draw-path (cdr path) cycle? #:v2? v2?))))

    (draw-edges #:draw-weights? (if is-task? #t #f))
    (draw-vertices)
    (when cycle
      (draw-path cycle #t))
    (set-appropriate-font text-font)
    (when left-top
      (foldl (lambda (msg offset)
               (send dc draw-text msg 4 offset)
               (+ offset 16))
             4
             left-top))
    (set-appropriate-font 13)
    (when path
      (draw-path path #f))
    (when path2
      (draw-path path2 #f #:v2? #t))))

(define (parse logfile)
  (define (get-task)
    (let ([task (read)])
     (if (not (equal? (car task) 'task))
       (error "error in logfile: expected task")
       (cdr task))))

  (define (get-init)
    (let ([init (read)])
     (if (not (equal? (car init) 'initial))
       (error "error in logfile: expected initial population")
       (cdr init))))

  (define (get-tail)
    (define (iterator result)
      (let ([i (read)])
       (cond [(equal? (car i) 'it)
              (iterator (cons (cdr i) result))]
             [(equal? (car i) 'result)
              (if (null? result)
                (error "error in logfile: expected iterations, got result")
                (list (reverse result) (cdr i)))]
             [else
              (error "error in logfile: unexpected keyword")])))

    (iterator '()))

  (with-input-from-file logfile
                        #:mode 'text
                        (lambda ()
                          (list* (get-task) (get-init) (get-tail)))))

(define (transform-sort-iterations task iterations)
  (map (lambda (i)
         (sort (foldl (lambda (new old)
                        (if (list? (car new))
                          (cons (make-chromosome task (seventh new) (+ 1 (length old)))
                                (cons (make-chromosome task (sixth new) (length old))
                                      old))
                          (cons (make-chromosome task new (length old)) old)))
                      '()
                      (cdr i))
               >
               #:key (lambda (c) (chromosome->fitness c))
               #:cache-keys? #t))
       iterations))

(define (visualize-solution logfile)
  (let* ([t (parse logfile)]
         [task-l (car t)]
         [task (make-task (car task-l) (cadr task-l) (caddr task-l))]
         [sol (call-with-values (lambda () (apply values (cadddr t))) make-sol)]
         [init (caadr t)]
         [iterations (caddr t)]
         [iterations-n (length iterations)]
         [iterations-sorted-transformed (transform-sort-iterations task iterations)])

    (define (draw-chromosome choice event)
      (clear-content-panel)
      (clear-parents-panel)
      (clear-crossover-panel)
      (clear-mutation-panel)
      (send content-panel add-child chromosome-parents-panel)
      (send content-panel add-child chromosome-crossover-panel)
      (send content-panel add-child chromosome-mutation-panel)

      (let* ([it-i (- (send iteration-selector get-selection) 1)]
             [it (cdr (list-ref iterations it-i))]
             [sorted-it (list-ref iterations-sorted-transformed it-i)]
             [ci (chromosome->i (list-ref sorted-it (- (send choice get-selection) 1)))]
             [c-pair (list-ref it (quotient ci 2))])
        (if (not (list? (car c-pair))) ; chromosome without a pair
          (new canvas%
             [parent chromosome-parents-panel]
             [paint-callback
              (lambda (canvas dc)
                (draw-graph task dc #:solution c-pair
                            #:left-top (list "This chromosome was the best in previous iteration:"
                                             (string-append "Fitness: " (number->string (chromosome->fitness (make-chromosome task c-pair 0)))))))]
             [min-width GRAPH-CANVAS-W]
             [min-height GRAPH-CANVAS-H]
             [style '(border)])
          (let* ([p0 (car c-pair)]
                 [p1 (cadr c-pair)]
                 [crossover (caddr c-pair)]
                 [crossover? (car crossover)]
                 [cross-path0 (if crossover? (cadr crossover) #f)] ; swap this
                 [cross-path1 (if crossover? (caddr crossover) #f)] ; and this
                 [cross-result0 (if crossover? (cadddr crossover) p0)]
                 [cross-result1 (if crossover? (last crossover) p1)]
                 [mutation0 (fourth c-pair)]
                 [mutation0? (car mutation0)]
                 [mutation1 (fifth c-pair)]
                 [mutation1? (car mutation1)]
                 [mutation-path00 (if mutation0? (car mutation0) #f)] ; replacing this
                 [mutation-path01 (if mutation0? (cadr mutation0) #f)] ; with this
                 [mutation-path10 (if mutation1? (car mutation1) #f)]
                 [mutation-path11 (if mutation1? (cadr mutation1) #f)]
                 [mutation-result0 (sixth c-pair)]
                 [mutation-result1 (last c-pair)]
                 )

        (new canvas% ; p0
             [parent chromosome-parents-panel]
             [paint-callback
              (lambda (canvas dc)
                (draw-graph task dc #:solution p0
                            #:path (if crossover? cross-path0 #f)
                            #:left-top (list "Parent 0:"
                                             (if crossover?
                                               (string-append (foldl
                                                                (lambda (new result)
                                                                  (string-append result " " (symbol->string new)))
                                                                "Path to swap: ("
                                                                cross-path0) ")")
                                               "No crossover on this step")
                                             (string-append "Fitness: " (number->string (chromosome->fitness (make-chromosome task p0 0)))))))]
             [min-width CHROMOSOME-CANVAS-W]
             [min-height CHROMOSOME-CANVAS-H]
             [style '(border)])

        (new canvas% ; p1
             [parent chromosome-parents-panel]
             [paint-callback
              (lambda (canvas dc)
                (draw-graph task dc #:solution p1
                            #:path (if crossover? cross-path1 #f)
                            #:left-top (list "Parent 1:"
                                             (if crossover?
                                               (string-append (foldl
                                                                (lambda (new result)
                                                                  (string-append result " " (symbol->string new)))
                                                                "Path to swap: ("
                                                                cross-path1) ")")
                                               "")
                                             (string-append "Fitness: " (number->string (chromosome->fitness (make-chromosome task p1 0)))))))]
             [min-width CHROMOSOME-CANVAS-W]
             [min-height CHROMOSOME-CANVAS-H]
             [style '(border)])

        (new canvas% ; i0
             [parent chromosome-crossover-panel]
             [paint-callback
              (lambda (canvas dc)
                (draw-graph task dc #:solution cross-result0
                            #:path (if mutation0? mutation-path00 #f)
                            #:path2 (if mutation0? mutation-path01 #f)
                            #:left-top (list "Intermediate 0:"
                                             (if mutation0?
                                               (string-append (foldl
                                                                (lambda (new result)
                                                                  (string-append result " " (symbol->string new)))
                                                                "Path to replace: ("
                                                                mutation-path00) ")")
                                               "No mutation")
                                             (if mutation0?
                                               (string-append (foldl
                                                                (lambda (new result)
                                                                  (string-append result " " (symbol->string new)))
                                                                "Path to emplace: ("
                                                                mutation-path01) ")")
                                               "on this step")
                                             (string-append "Fitness: " (number->string (chromosome->fitness (make-chromosome task cross-result0 0)))))))]
             [min-width CHROMOSOME-CANVAS-W]
             [min-height CHROMOSOME-CANVAS-H]
             [style '(border)])

        (new canvas% ; i1
             [parent chromosome-crossover-panel]
             [paint-callback
              (lambda (canvas dc)
                (draw-graph task dc #:solution cross-result1
                            #:path (if mutation1? mutation-path10 #f)
                            #:path2 (if mutation1? mutation-path11 #f)
                            #:left-top (list "Intermediate 1:"
                                             (if mutation1?
                                               (string-append (foldl
                                                                (lambda (new result)
                                                                  (string-append result " " (symbol->string new)))
                                                                "Path to replace: ("
                                                                mutation-path10) ")")
                                               "No mutation")
                                             (if mutation1?
                                               (string-append (foldl
                                                                (lambda (new result)
                                                                  (string-append result " " (symbol->string new)))
                                                                "Path to emplace: ("
                                                                mutation-path11) ")")
                                               "on this step")
                                             (string-append "Fitness: " (number->string (chromosome->fitness (make-chromosome task cross-result1 0)))))))]
             [min-width CHROMOSOME-CANVAS-W]
             [min-height CHROMOSOME-CANVAS-H]
             [style '(border)])

        (new canvas% ; res0
             [parent chromosome-mutation-panel]
             [paint-callback
              (lambda (canvas dc)
                (draw-graph task dc #:solution mutation-result0
                            #:left-top (list "Result 0:"
                                             (string-append "Fitness: " (number->string (chromosome->fitness (make-chromosome task mutation-result0 0)))))))]
             [min-width CHROMOSOME-CANVAS-W]
             [min-height CHROMOSOME-CANVAS-H]
             [style '(border)])

        (new canvas% ; res1
             [parent chromosome-mutation-panel]
             [paint-callback
              (lambda (canvas dc)
                (draw-graph task dc #:solution mutation-result1
                            #:left-top (list "Result 1:"
                                             (string-append "Fitness: " (number->string (chromosome->fitness (make-chromosome task mutation-result1 0)))))))]
             [min-width CHROMOSOME-CANVAS-W]
             [min-height CHROMOSOME-CANVAS-H]
             [style '(border)])
        ))))

    ; static panels
    (define frame (new frame%
                       [label "Visualization"]
                       [width 800]
                       [height 600]))

    (define top-panel (new vertical-panel% [parent frame] [stretchable-height #f] [alignment '(center center)]))
    (define menu-panel (new horizontal-panel% [parent top-panel] [alignment '(left top)]))
    (define left-menu (new horizontal-panel% [parent menu-panel] [alignment '(left top)]))
    (define right-menu (new horizontal-panel% [parent menu-panel] [alignment '(right top)]))
    (define content-panel (new horizontal-panel% [parent top-panel] [alignment '(left top)]))

    ; functions for element callbacks
    (define (clear-left-menu)
      (send left-menu change-children (lambda (x) '())))

    (define (clear-parents-panel)
      (send chromosome-parents-panel change-children (lambda (x) '())))

    (define (clear-crossover-panel)
      (send chromosome-crossover-panel change-children (lambda (x) '())))

    (define (clear-mutation-panel)
      (send chromosome-mutation-panel change-children (lambda (x) '())))

    (define (clear-content-panel)
      (send content-panel change-children (lambda (x) '())))

    (define (activate-global-mode btn event)
      (clear-left-menu)
      (send left-menu add-child result-desc-btn)
      (send left-menu add-child best-mid-plot-btn)
      ;(send left-menu add-child gen-plot-btn)
      )

    (define (activate-iteration-mode choice event)
      (clear-left-menu)
      (send iteration-menu-panel change-children (lambda (x) '()))
      (send left-menu add-child iteration-menu-panel)
      (let* ([it-i (- (send iteration-selector get-selection) 1)]
             [it (list-ref iterations-sorted-transformed it-i)]
             [chromosomes-n (length it)])
        (new choice%
             [parent iteration-menu-panel]
             [label "Select"]
             [callback draw-chromosome]
             [choices (cons "chromosome"
                            (build-list chromosomes-n (lambda (i)
                                                        (string-append (number->string i)
                                                                       ": "
                                                                       (number->string (chromosome->fitness (list-ref it i)))))))])))

    ; functional elements
    (new button% [parent right-menu] [label "Global"] [callback activate-global-mode])
    (define gen-plot-btn (new button%
                              [parent left-menu]
                              [label "3D plot"]
                              [callback (lambda (btn event)
                                          (clear-content-panel)
                                          (send content-panel add-child gen-plot-panel)
                                          )]
                              [style '(deleted)]))
    (define best-mid-plot-btn (new button%
                                   [parent left-menu]
                                   [label "Population characteristics"]
                                   [callback (lambda (btn event)
                                               (clear-content-panel)
                                               (send content-panel add-child best-mid-panel)
                                               )]
                                   [style '(deleted)]))
    (define result-desc-btn (new button%
                                 [parent left-menu]
                                 [label "Result"]
                                 [callback (lambda (btn event)
                                             (clear-content-panel)
                                             (send content-panel add-child result-desc-panel))]
                                 [style '(deleted)]))

    (define result-desc-panel (new horizontal-panel%
                                   [parent content-panel]
                                   [alignment '(left top)]
                                   ;[stretchable-width #f]
                                   #|[style '(deleted)]|#))

    (define task-canvas (new canvas%
                             [parent result-desc-panel]
                             [paint-callback
                              (lambda (canvas dc)
                                (draw-graph task dc #:is-task? #t
                                            #:left-top (list "Task"
                                                             (string-append "max weight: "
                                                                            (number->string (task->max-weight task))))
                                            #:text-font 13))]
                             [min-width GRAPH-CANVAS-W]
                             [min-height GRAPH-CANVAS-H]
                             [style '(border)]))

    (define solution-canvas (new canvas%
                                 [parent result-desc-panel]
                                 [paint-callback
                                  (lambda (canvas dc)
                                    (if (sol->ans sol)
                                      (draw-graph task dc #:is-task? #f #:solution (sol->path sol)
                                                  #:left-top (list "Solution"
                                                                   (string-append "path weight: "
                                                                                  (number->string (/ 1 (chromosome->fitness (make-chromosome task (sol->path sol) 0))))))
                                                  #:text-font 13)
                                      (send dc draw-text "solution: #f" 4 4) ))
                                    ]
                                 [min-width GRAPH-CANVAS-W]
                                 [min-height GRAPH-CANVAS-H]
                                 [style '(border)]))

    (define gen-plot-panel (new horizontal-panel%
                                [parent content-panel]
                                [alignment '(left top)]
                                ;[stretchable-width #f]
                                [style '(deleted)]))

    (define gen-plot-canvas (new canvas%
                                 [parent gen-plot-panel]
                                 [paint-callback
                                  (lambda (canvas dc)
                                    (plot3d/dc (surface3d (lambda (it c)
                                                            (printf "i: ~a, c: ~a~n" it c)
                                                            1)
                                                          0 iterations-n
                                                          0 100
                                                          #:z-min -10 #:z-max 10
                                                          #:samples 10)
                                               dc 0 0 600 600
                                               #:title "General plot"
                                               #:x-label "iteration" #:y-label "chromosome" #:z-label "fitness"))]
                                 [min-width GRAPH-CANVAS-W]
                                 [min-height GRAPH-CANVAS-H]
                                 [style '(border)]))

    (define best-mid-panel (new horizontal-panel%
                                [parent content-panel]
                                [alignment '(left top)]
                                ;[stretchable-width #f]
                                [style '(deleted)]))

    (define best-mid-plot-canvas (new editor-canvas%
                                      [parent best-mid-panel]
                                      [editor (new pasteboard%)]
                                      [min-width GRAPH-CANVAS-W]
                                      [min-height GRAPH-CANVAS-H]))

    (send (send best-mid-plot-canvas get-editor) set-dragable #f)
    (send (send best-mid-plot-canvas get-editor) insert
          (plot-snip (list (function (lambda (it) ; best
                                       (let ([fit (chromosome->fitness (car (list-ref iterations-sorted-transformed it)))])
                                        (if (< fit 0)
                                          fit
                                          fit
                                          #|(* fit (task->max-weight task))|#)))
                                     0 (- iterations-n 1)
                                     ;#:y-min -10
                                     ;#:y-max 10
                                     #:width 2
                                     #:color (send the-color-database find-color "DarkGreen")
                                     #:samples iterations-n
                                     #:label "best chromosome")
                           (function (lambda (it) ; avg
                                       (let* ([iteration (list-ref iterations-sorted-transformed it)]
                                              [fit (foldl (lambda (c res) (+ res (chromosome->fitness c))) 0 iteration)]
                                              [res (/ fit (length iteration))])
                                         res))
                                     0 (- iterations-n 1)
                                     ;#:y-min -10
                                     ;#:y-max 10
                                     #:width 2
                                     #:color (send the-color-database find-color "LightSlateGray")
                                     #:samples iterations-n
                                     #:label "average chromosome")
                           (function (lambda (it) ; worst
                                       (let ([fit (chromosome->fitness (last (list-ref iterations-sorted-transformed it)))])
                                        (if (< fit 0)
                                          fit
                                          fit
                                          #|(* fit (task->max-weight task))|#)))
                                     0 (- iterations-n 1)
                                     ;#:y-min -10
                                     ;#:y-max 10
                                     #:width 2
                                     #:color (send the-color-database find-color "Red")
                                     #:samples iterations-n
                                     #:label "worst chromosome"))
                     #:title "Characteristics"
                     #:width 500 #:height 500
                     #:x-label "iteration" #:y-label "fitness"))

    #|(define result-desc-weight (new message%
                                      [label (string-append "max. weight: " (number->string (task->max-weight task)))]
                                      [parent result-desc-panel]))|#

    (define iteration-menu-panel (new horizontal-panel%
                                      [parent left-menu]
                                      [alignment '(left top)]
                                      ;[stretchable-width #f]
                                      [style '(deleted)]))

    (define iteration-selector (new choice%
                                    [parent right-menu]
                                    [label "or select"]
                                    [callback activate-iteration-mode]
                                    [choices (cons "iteration" (build-list iterations-n number->string))]))

    (define chromosome-parents-panel (new vertical-panel%
                                          [parent content-panel]
                                          ;[min-width CHROMOSOME-CANVAS-W]
                                          ;[stretchable-width #f]
                                          [alignment '(left top)]))

    (define chromosome-crossover-panel (new vertical-panel%
                                            [parent content-panel]
                                            ;[min-width CHROMOSOME-CANVAS-W]
                                            ;[stretchable-width #f]
                                            [alignment '(left top)]))

    (define chromosome-mutation-panel (new vertical-panel%
                                           [parent content-panel]
                                           ;[min-width CHROMOSOME-CANVAS-W]
                                           ;[stretchable-width #f]
                                           [alignment '(left top)]))


    (send frame show #t)))

(visualize-solution "rpp_result0")
